@extends('layouts.dashboard.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Blank Page
            </h1>

            <ol class="breadcrumb">
                <li class="activ"><i class="fa fa-dashboard"></i>@lang('site.dashboard')</li>
            </ol>
        </section>

        <section class="content">
            <h1>This is dashboard</h1>
        </section>
    </div>
@endsection
